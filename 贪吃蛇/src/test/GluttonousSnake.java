package test;



import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import role.Food;
import role.SnakeNode;

/**
 * 贪吃蛇的主窗口程序
 * @author Herrona
 *
 */
public class GluttonousSnake extends JFrame{
	SnakeNode head;//头结点
	Food bean;//豆子
	int score;//分数
	int length;//蛇的长度
	Rectangle validity=new Rectangle(30,40,440,415);//游戏的有效区域
	List<SnakeNode> snake=new ArrayList<SnakeNode>();
	Logic logic = new Logic();
	GluttonousSnake(){
		initComponents();
		initFrame();
		//开启线程 
		logic.start();
	}
	//初始化窗口中的组件（角色）
	void initComponents(){
		Random random = new Random();
		//初始蛇
		head = new SnakeNode();
		int birthX=0;
		int birthY=0;
		do
		{
			birthX=random.nextInt(40)*10;
			birthY=random.nextInt(40)*10;
		}while(!validity.contains(birthX, birthY));	
		head.setX(birthX);
		head.setY(birthY);
		SnakeNode.direction=random.nextInt(4);//随机出生移动方向  可要可不要
		head.setColor(Color.black);
		snake.add(head);
		
		//初始蛇身
		SnakeNode body=null;
		int birthCount = 5; //初始长度
		for(int i=1 ;i < birthCount-1 ; i++){
			body=new SnakeNode();
			switch(SnakeNode.direction){
			case 0 : 
				body.setX(snake.get(i-1).getX());
				body.setY(snake.get(i-1).getY()+10);
				break;
			case 1 :
				body.setX(snake.get(i-1).getX()-10);
				body.setY(snake.get(i-1).getY());
				break;
			case 2 : 
				body.setX(snake.get(i-1).getX());
				body.setY(snake.get(i-1).getY()-10);
				break;
			case 3 : 
				body.setX(snake.get(i-1).getX()+10);
				body.setY(snake.get(i-1).getY());
				break;
			}
			snake.add(body);
		}
		length=snake.size();
		//初始豆子
		bean=new Food();
		int beanX=0;
		int beanY=0;
		do{
			beanX=random.nextInt(40)*10;
			beanY=random.nextInt(40)*10;
		}while(!validity.contains(beanX, beanY));
		bean.setX(beanX);
		bean.setY(beanY);
	}
	
	/**
	 * 初始化窗口
	 */
	void initFrame(){
		this.setTitle("贪吃蛇");
		this.setIconImage(new ImageIcon("images/icon.png").getImage());
		JLabel lab=new JLabel( new ImageIcon("images/bg.png"));
		this.add(lab);
		this.setSize(500, 500);
		this.setUndecorated(true);
		this.setLocationRelativeTo(null);
		
		//添加键盘事件，上下左右控制蛇的方向和ESC退出游戏
		addKeyListener(new KeyAdapter(){
			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub
				if(e.getKeyCode()==KeyEvent.VK_DOWN){
					if(SnakeNode.direction!=SnakeNode.UP)
						SnakeNode.direction=SnakeNode.DOWN;
				}else if(e.getKeyCode()==KeyEvent.VK_UP){
					if(SnakeNode.direction!=SnakeNode.DOWN)
						SnakeNode.direction=SnakeNode.UP;
				}else if(e.getKeyCode()==KeyEvent.VK_LEFT){
					if(SnakeNode.direction!=SnakeNode.RIGHT)
						SnakeNode.direction=SnakeNode.LEFT;
				}else if(e.getKeyCode()==KeyEvent.VK_RIGHT){
					if(SnakeNode.direction!=SnakeNode.LEFT)
						SnakeNode.direction=SnakeNode.RIGHT;
				}else if(e.getKeyCode() == KeyEvent.VK_ESCAPE){
					logic.suspend();//暂停
					if(JOptionPane.showConfirmDialog(null, "您确定要退出游戏吗？")==0)
						System.exit(0);
					else
						logic.resume();//恢复
				}
			}		
		});
	}

	/**
	 * 绘制窗口
	 */
	public void paint(Graphics g) {
		super.paint(g);//不调用此方法，原先的图像不会消失
		
		//画豆子
		g.setColor(bean.getColor());
		g.drawOval(bean.getX(),bean.getY(), bean.getWidth(), bean.getHeight());
		g.fillOval(bean.getX(),bean.getY(), bean.getWidth(), bean.getHeight());
		
		//画蛇
		for(SnakeNode s : snake){
			g.setColor(s.getColor());
			g.drawRect(s.getX(),s.getY(), s.getWidth(), s.getHeight());
			g.fillRect(s.getX(),s.getY(), s.getWidth(), s.getHeight());
		}
		//显示分数
		g.drawString("得分：", 350, 60);
		g.setColor(Color.red);
		g.drawString(""+score*10, 385, 60);
	}
	
	public static void main(String[] args) {
		GluttonousSnake s = new GluttonousSnake();
		s.setVisible(true);
	}
	
	/**
	 * 游戏逻辑线程 绘制逻辑与游戏规则(核心代码)
	 * @author Herrona
	 */
	private class Logic extends Thread{
		Random random = new Random();
		public void run() {
			int x=0,y=0;
			while(true){
				x=head.getX();
				y=head.getY();
				Rectangle head = new Rectangle(x,y,10,10);
				Point[] temp = new Point[length];//保存所有节点的当前状态
				Rectangle[] rec = new Rectangle[length]; //保存所有节点的矩形区域
				//获得所有节点的当前状态
				for(int i = 0;i<temp.length;i++){
					temp[i]=new Point(snake.get(i).getX(),snake.get(i).getY());
					rec[i]=new Rectangle(temp[i].x,temp[i].y,snake.get(i).getWidth(),snake.get(i).getHeight());
				}
				
				//撞到自己
				for(int i = 2; i < rec.length ; i++) {
					//判断依据：头部和食物的交集不为空
					if(!rec[i].intersection(head).isEmpty())
					{
						new OptionPane("贪吃蛇吃到了自己！游戏结束！得分：" + score*10).setVisible(true);
						stop();
						break;
					}
				}
				//设置蛇身坐标，依次前移
				for(int i = 1;i < snake.size() ;i++){
					snake.get(i).setX((int) temp[i-1].getX());
					snake.get(i).setY((int) temp[i-1].getY());
				}
				//判断前进方向，设置头部状态
				switch(SnakeNode.direction){
					case 0 : 
						GluttonousSnake.this.head.setY(y-10);
						break;
					case 1 :
						GluttonousSnake.this.head.setX(x+10);
						break;
					case 2 : 
						GluttonousSnake.this.head.setY(y+10);
						break;
					case 3 : 
						GluttonousSnake.this.head.setX(x-10);
						break;
				}
				//吃豆 碰撞检测
				Rectangle r1=new Rectangle(x,y,10,10);
				Rectangle r2=new Rectangle(bean.getX(),bean.getY(),10,10);
				if(r2.intersects(r1)){
					score++;
					SnakeNode s = new SnakeNode();
					//s.setColor(Color.darkGray);
					snake.add(s);
					s.setY(temp[length-1].y);
					s.setX(temp[length-1].x);
					length=snake.size();
					//改变豆子位置
					int beanX=0;
					int beanY=0;
					do {
						beanX=random.nextInt(40)*10;
						beanY=random.nextInt(40)*10;
					}
					while(!validity.contains(beanX, beanY));
			
					bean.setX(beanX);
					bean.setY(beanY);
				}
				//边界检测
				//if(x < 0+30 || y<0+30 || x>(500-30) || y >(500-30))
				if(!validity.contains(x, y))
				{
					new OptionPane("贪吃蛇撞墙啦！游戏结束！得分：" + score*10).setVisible(true);
					stop();
				};
				repaint();
				try {
					//50 100 200 300 超快  快 中 慢            低于35出现闪烁 
					sleep(200);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		
	}
	
	/**
	 * 对话框
	 * @author Herrona
	 */
	private class OptionPane extends JDialog{
		JButton restart;
		JButton exit;
		JButton back;
		JLabel msgLab = new JLabel();
		OptionPane(String msg){
			msgLab.setText(msg);
			initCompinents();
			initOptionPane();
		}
		
		void initCompinents(){
			restart = new JButton("重新开始");
			restart.addActionListener(new ActionListener(){
				@Override
				public void actionPerformed(ActionEvent e) {
					// TODO Auto-generated method stub
					new GluttonousSnake().setVisible(true);
					OptionPane.this.dispose();
					GluttonousSnake.this.dispose();
				}
			});
			exit =  new JButton("退出");
			exit.addActionListener(new ActionListener(){
				@Override
				public void actionPerformed(ActionEvent e) {
					// TODO Auto-generated method stub
					System.exit(0);
				}
			});
			back =  new JButton("返回");
			back.addActionListener(new ActionListener(){
				@Override
				public void actionPerformed(ActionEvent e) {
					OptionPane.this.dispose();
				}
			});
		}
		
		void initOptionPane(){
			this.setLayout(new FlowLayout());
			this.setSize(300,100);
			setTitle("提示");
			add(msgLab);
			add(restart);
			add(exit);
			add(back);
			this.setLocationRelativeTo(null);
		}
	}
}
